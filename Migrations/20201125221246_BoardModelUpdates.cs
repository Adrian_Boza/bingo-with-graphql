﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BingoGraphQL.Migrations
{
    public partial class BoardModelUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "finished",
                table: "boards",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int[]>(
                name: "getted_numbers",
                table: "boards",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "players_number",
                table: "boards",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "finished",
                table: "boards");

            migrationBuilder.DropColumn(
                name: "getted_numbers",
                table: "boards");

            migrationBuilder.DropColumn(
                name: "players_number",
                table: "boards");
        }
    }
}
