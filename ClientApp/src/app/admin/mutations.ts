import gql from 'graphql-tag';

 export const CREATE_BOARD = gql`
    mutation($input: BoardInput!){
        createBoard(input: $input){
                id
                nameBoard
                adminUrl
                url
        }
  
    }
`;


export const DELETE_BOARD = gql`
mutation($id: ID!){
    deleteBoard(id: $id){
        id
    }

}
`;