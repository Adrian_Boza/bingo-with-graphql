import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Board } from '../_models/board'
import { Router } from '@angular/router'
import { CREATE_BOARD, DELETE_BOARD } from './mutations'
import { GET_BOARDS } from '../admin/queries'
import { Apollo } from 'apollo-angular'
import { from } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {
 
  boards : Board[] = [];
  id: string;  
  urlAdmi = "https://localhost:5001/AdminBoard/"
  urlBoard = "https://localhost:5001/Board/"
  boardForm = new FormGroup({

    id : new FormControl('',Validators.required),
    nameBoard : new FormControl('',Validators.required)

  })
   

  constructor(private apollo: Apollo, private router : Router) {

   }

  ngOnInit() {
    
    this.filter(0);
    this.id = localStorage.getItem('token'); 
    
  }

  filter(num:number) {
    this.apollo.watchQuery({
      query: GET_BOARDS,
      fetchPolicy: 'network-only',
      variables: {
        orderBy:num
      }

    }).valueChanges.subscribe(result => {
      this.boards = result.data['boards'];
    });
  }

  
  generateBoards(form:Board){
    
    const result =this.boards.filter(p => p.id == form.id);
    if(result.length == 0 && form.id != null && form.nameBoard != ""){
      let mutation = CREATE_BOARD;
      debugger
      console.log(form.nameBoard)
      const variables = {
        input: { id: form.id, nameBoard: form.nameBoard, adminUrl: this.urlAdmi + form.id, url: this.urlBoard + form.id,
          gettedNumbers: [], finished: false, playersNumber: 0}
          
      };

      this.apollo.mutate({
        mutation: mutation,
        variables: variables
      }).subscribe(() => {
        this.boardForm.reset();
        this.filter(0);

      });
    }else{
      console.log("esta en el arreglo");
    }
  }


  deleteBoard(board: Board){
    
    this.apollo.mutate({
      mutation: DELETE_BOARD,
      variables: { id: board.id }
    }).subscribe(() => {
      this.filter(0);
      
    });
  }


  logout() {  
    
    this.out();  
    this.router.navigate(['/login']);  
  }  
  
  out (): void  {    
    localStorage.setItem ( 'isLoggedIn' , 'falso' );    
    localStorage.removeItem ( 'token' );    
  } 
}
