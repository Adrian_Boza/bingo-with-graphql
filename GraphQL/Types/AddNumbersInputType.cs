using GraphQL.Types;
using BingoGraphQL.Models;

namespace  BingoGraphQL.GraphQl.Types
{
    class AddNumbersInputType : InputObjectGraphType
    {
        public AddNumbersInputType()
        {
            Name = "AddNumbersInput";
            Field<StringGraphType>("nameBoard");
            Field<StringGraphType>("url");
            Field<StringGraphType>("AdminUrl");
            Field<IntGraphType>("PlayersNumber");
            Field<NonNullGraphType<ListGraphType<IntGraphType>>>("GettedNumbers");
            Field<BooleanGraphType>("Finished");
           
        }
    }
}