export class Board {
  id: number;
  nameBoard: string;
  url : string;
  adminUrl : string;
  gettedNumbers: [];
  playersNumber:0;
  finished: false;
}
