using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using GraphQL.Types;
using BingoGraphQL.Models;
using Microsoft.AspNetCore.Mvc;

namespace BingoGraphQL.Repositories
{
    class AdminRepository 
    {
        private readonly DataBaseContext _context;
        public AdminRepository(DataBaseContext context)
        {
            _context = context;
        }

        public Administrator Auth(Administrator admin) {

            var account = _context.Administrators.SingleOrDefault(x => x.Email == admin.Email);

            bool isValidPassword = BCrypt.Net.BCrypt.Verify(admin.PasswordHash, account.PasswordHash);
            if(isValidPassword){
             
              return account;
            }

            return null;

        }

         public async Task<Administrator> Add(Administrator admin) {
            
            admin.PasswordHash = BCrypt.Net.BCrypt.HashPassword(admin.PasswordHash);
            _context.Administrators.Add(admin);
            await _context.SaveChangesAsync();
            return admin;
            
        }

    }
}