using System;

namespace BingoGraphQL.Models
{
    interface IStream
    {
        Board AddStream(Board board);
        IObservable<Board> StreamBoard();
    }
}