using GraphQL.Types;
using BingoGraphQL.GraphQl.Types;
using BingoGraphQL.Models;
using GraphQL.Resolvers;
using System;

namespace BingoGraphQL.GraphQl
{
    class AppSubscription : ObjectGraphType
    {   
        private readonly IStream _stream;
        public AppSubscription(IStream stream)
        {    
            _stream = stream;
            AddField(new EventStreamFieldType
            {
                Name = "boardAdded",
                Type = typeof(BoardType),
                Resolver = new FuncFieldResolver<Board>(ResolveBoard),
                Subscriber = new EventStreamResolver<Board>(Subscribe)
            });
    

        }
         private Board ResolveBoard(ResolveFieldContext<object> context){
            var board = context.Source as Board;
            return board;
        }

        private IObservable<Board> Subscribe(ResolveFieldContext<object> context){
            return _stream.StreamBoard();
        }
      
    }
}