using System;

namespace BingoGraphQL.Models
{
    public class Board
    {
        public int Id { get; set; }
        public string NameBoard { get; set; }
        public string Url { get; set; }
        public string AdminUrl {get; set;}
        public int PlayersNumber { get; set; }
        public int[] GettedNumbers { get; set; }
        public bool Finished { get; set; }
       

    }

}