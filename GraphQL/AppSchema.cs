
using GraphQL.Types;
using GraphQL;

namespace BingoGraphQL.GraphQl
{
    class AppSchema : Schema
    {
        public AppSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<AppQuery>(); 
            Mutation = resolver.Resolve<AppMutation>(); 
            Subscription = resolver.Resolve<AppSubscription>(); 
        }
    }

}