import { Component, Input, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Board } from '../_models/board'
import { Card } from '../interfaces/card.interface';
import { GET_SUBSCRIPTION } from './Subscription';
import { Apollo } from 'apollo-angular';
import { UPDATE_BOARD } from './mutations'
import { GET_BOARD } from './queries'


@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent {
  
  @HostListener('window:unload', ['$event'])
  unloadHander() {
    this.onStreamPagine();
  }
 
 
  boards: Board[] = [];
  id: number;
  @Input() CurrentNumber: number;
  @Input() RecentNumbers: number[] = [];
  lastPost: Board;
  newPlayer:boolean;
  public numbers;
  public all_nums: number[];
  public gettedNumbers: number[];
  public cards: Card[];
  public cardsValided: Card[];
  public counter: number;
  public showCounterForm: boolean;
  public win: boolean;
  public gameF: boolean;
  public cardsC: boolean;
  public recent1: number;
  public recent2: number;
  public recent3: number;
  public currentN: number;
  public nameBoard;
  public adminUrl;
  public url;
  public playersNumber;
  public localCards;
  public boardExist;
 

  constructor(private apollo: Apollo, private route: ActivatedRoute) {
    this.boardExist = true;
    this.newPlayer = false;
    
    this.route.params.subscribe(params => {

      this.id = Number(params['id']);
      this.getBoard(this.id);
     
    })
    this.apollo.subscribe({
      query: GET_SUBSCRIPTION,
    }).subscribe(result => {
      if (this.id == result.data['boardAdded'].id) {
        this.id = result.data['boardAdded'].id;
        this.gettedNumbers = result.data['boardAdded'].gettedNumbers;
        this.gameF = result.data['boardAdded'].finished;
        this.recent1 = this.gettedNumbers[this.gettedNumbers.length - 2];
        this.recent2 = this.gettedNumbers[this.gettedNumbers.length - 3];
        this.recent3 = this.gettedNumbers[this.gettedNumbers.length - 4];
        this.currentN = this.gettedNumbers[this.gettedNumbers.length - 1];
        this.url = result.data['boardAdded'].url;
        this.adminUrl = result.data['boardAdded'].adminUrl;
        this.nameBoard = result.data['boardAdded'].nameBoard;
        this.playersNumber = result.data['boardAdded'].playersNumber;
        this.game();
      }


    })
    this.unloadHander();
    this.cards = [];
    this.counter = 1;
    this.showCounterForm = true;
    this.all_nums = [];
    this.cardsC = false;

    for (let index = 1; index < 91; index++) {
      this.all_nums.push(index);

    }
    this.cardsValided = JSON.parse(localStorage.getItem(JSON.stringify(this.id)));
    if(this.cardsValided != null){
      if (this.cardsValided['0'].idBoard != this.id) {
        this.localCards = false;
      }else{
       
        this.localCards = true;
        this.cards = JSON.parse(localStorage.getItem(JSON.stringify(this.id)));
        this.win = JSON.parse(localStorage.getItem('win'));
        
      }
    }else{
      this.localCards = false;
    }
   


  }
  createCards() {
    debugger
    if (this.localCards == false) {
      for (let index = 1; index < this.counter + 1; index++) {
        var newCard: Card = {
          'id': index,
          'numbers': this.genCardNumbers(),
          'completed': false,
          'idBoard': this.id
        }
        this.cards.push(newCard);
      }
      localStorage.setItem(JSON.stringify(this.id), JSON.stringify(this.cards));
      this.showCounterForm = false;
      this.localCards = true;
    }



  }
  genCardNumbers() {
    var array: number[] = [];
    var arraySize = this.all_nums.length;
    for (let index = 0; index < 15;) {
      var random = Math.floor(Math.random() * (arraySize - 0)) + 0;
      if (!array.includes(random)) {
        array.push(random);
        index++;
      }
    }
    return array;
  }
  isGetted(num: number) {
    return this.gettedNumbers.includes(num);
  }
  isBiggerThan9(num: number) {
    if (num > 9) {
      return true
    }
    return false;
  }

  game() {
    if (!this.gameF) {
      this.cards.forEach(card => {
        var inThisCard: number[] = [];
        card.numbers.forEach(number => {
          if (this.gettedNumbers.includes(number)) {
            inThisCard.push(number);
          }
        });
        if (inThisCard.length == 15) {
          this.win = true;
          localStorage.setItem('win', JSON.stringify(this.win));
          this.gameF = true;
          this.save(this.id);

        }
      });
    }

  }
  save(idBoard) {
    let mutation = UPDATE_BOARD;
    const variables = {
      id: idBoard,
      input: {
        id: idBoard,
        nameBoard: this.nameBoard,
        adminUrl: this.adminUrl,
        url: this.url,
        gettedNumbers: this.gettedNumbers,
        finished: this.gameF,
        playersNumber: this.playersNumber
      }

    };
    this.apollo.mutate({
      mutation: mutation,
      variables: variables
    }).subscribe(() => {

    });
  }


  getBoard(idBoard) {
    
    this.apollo.watchQuery({
      query: GET_BOARD,
      fetchPolicy: 'network-only',
      variables: { id: idBoard }
 
    }).valueChanges.subscribe(result => {
        
        this.id = idBoard;
        this.gettedNumbers = result.data['board'].gettedNumbers; 
        this.playersNumber = result.data['board'].playersNumber;
        this.gameF = result.data['board'].finished;
        this.url = result.data['board'].url;
        this.adminUrl = result.data['board'].adminUrl;
        this.nameBoard = result.data['board'].nameBoard;
        if(this.newPlayer){
          this.updatePlayer()
          this.newPlayer = false
        }
        return this.boardExist = true

    }, error => (console.log(error)));

    return this.boardExist = false
  }
  closeGame() {
    localStorage.removeItem('cards');
    localStorage.removeItem('win');
    debugger
    window.close();
  }


  updatePlayer(){
    this.playersNumber += 1;
    this.save(this.id)
  }
  deletePlayer(){
    this.playersNumber -= 1;
    this.save(this.id)
    if(confirm("Are you sure?")){
      this.closeGame()
    }
    
  }

  onStreamPagine(){
    if(confirm("Welcome Are you new player?")){
      this.newPlayer = true
    }else{
      this.newPlayer = false
    }
  }
}