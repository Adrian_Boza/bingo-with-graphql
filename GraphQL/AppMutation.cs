using GraphQL.Types;
using BingoGraphQL.Repositories;
using BingoGraphQL.GraphQl.Types;
using BingoGraphQL.Models;
using GraphQL;

namespace BingoGraphQL.GraphQl
{
    class AppMutation : ObjectGraphType
    {
        public AppMutation(AdminRepository adminRepository, BoardRepository boardRepository, IStream stream)
        {

            Field<AdminType>("createAdmin",
                              arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AdminInputType>> { Name = "input" }),
                              resolve: context => {
                                  return adminRepository.Add(context.GetArgument<Administrator>("input"));
                              });

            Field<BoardType>("createBoard",
                              arguments: new QueryArguments(new QueryArgument<NonNullGraphType<BoardInputType>> { Name = "input" }),
                              resolve: context => {
                                  
                                  return boardRepository.Add(context.GetArgument<Board>("input"));
                              });
            
            Field<BoardType>("updateBoard",
                              arguments: new QueryArguments(
                                  new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                                  new QueryArgument<NonNullGraphType<BoardInputType>> { Name = "input" }
                              ),
                              resolve: context => {
                                  var id = context.GetArgument<int>("id");
                                  var board = context.GetArgument<Board>("input");
                                  var subs = stream.AddStream(board);
                                  return boardRepository.AddNumbers(id, board);
                              });
            
            Field<BoardType>("deleteBoard",
                              arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                              resolve: context => {
                                  return boardRepository.Remove(context.GetArgument<int>("id"));
                              });
            
        }
    }
}