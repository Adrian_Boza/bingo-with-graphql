import gql from 'graphql-tag';

 export const GET_BOARDS = gql`
    query($orderBy: Int){
        boards(orderBy: $orderBy){
            id
            nameBoard
            adminUrl
            url
        }
  
    }
`;

