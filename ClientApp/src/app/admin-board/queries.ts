import gql from 'graphql-tag';

 export const GET_BOARD = gql`
    query($id: ID!){
        board(id: $id){
            id
            nameBoard
            adminUrl
            url
            gettedNumbers
            finished
            playersNumber
        }
  
    }
`;