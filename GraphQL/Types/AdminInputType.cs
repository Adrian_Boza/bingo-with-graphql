using GraphQL.Types;
using BingoGraphQL.Models;

namespace  BingoGraphQL.GraphQl.Types
{
    class AdminInputType : InputObjectGraphType
    {
        public AdminInputType()
        {
            Name = "AdminInput";
            Field<NonNullGraphType<StringGraphType>>("email");
            Field<NonNullGraphType<StringGraphType>>("passwordHash");
        }
    }
}