import gql from 'graphql-tag';

 export const GET_ADMIN = gql`
    query($input: AdminInput!){
      getAdmin(input: $input){
        firstName
        lastName
        email
        passwordHash
      }
    }
`;



